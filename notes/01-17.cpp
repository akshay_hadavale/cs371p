// -----------
// Wed, 17 Jan
// -----------

/*
Reminders
    Sun, 21 Jan: Blog  #1
    Sun, 21 Jan: Paper #1: Syllabus

    please, no phones, tablets, or laptops
    I'll publish these notes daily

    please check your Canvas grades regularly
    please be proactive on Ed Discussion

    UT Programming Club (UTPC)
    https://www.cs.utexas.edu/users/utpc/
    F, 26 Jan, 5:30 pm, GDC auditorium

    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/users/downing/cs104c/
    F, 2 pm, GDC 1.304

    UT Chess Club (UTCC)
    https://utexas.campuslabs.com/engage/organization/chessclubut/
    M, 6 pm, Dobie Center
*/

/*
class website
    https://www.cs.utexas.edu/users/downing/cs371p/
    Schedule
*/

/*
Canvas
    grades
    Lectures Online
    quizzes
*/

/*
Ed Discussion
    threaded questions
    private posts to staff
    14 blogs
*/

/*
GitLab
    examples
    exercises
    notes
    project submissions
*/

/*
Google
    project rubrics
*/

/*
HackerRank
    projects
    in-class exercises
*/

/*
Perusall
    14 papers
*/

/*
project, every 3 weeks, first two weeks from today
exercises, unpredictable
blogs, due Sun
    prompt on Fri
papers, due Sun
    post the paper Tue
quizzes, daily
*/

/*
5 projects
    first, individual in HackerRank
    remaining four in pairs
    can't have the same partner twice
*/

/*
12 in-class exercises
    almost weekly
    HackerRank
    in pairs
*/

/*
14 blogs
    weekly
    first and last
    Ed Discussion
    any platform (last term almost everyone used Medium)
*/

/*
14 papers
    weekly
    Perusall
*/

/*
42 quizzes
    daily
    Canvas
    multiple choice
    3 min
*/

/*
ice breaker
    name
    contact info
    what's your favorite programming language and why
    what led you to major in CS, when did you decide
    google "cs 371p fall 2023 final entry", click images
*/

/*
UTPC
    competitive programming

local contest
regional contests
world contests

2014: 2nd, Rice, 1st, Thailand
2015: 2nd, Rice, 1st, Morocco
2016: 1st, 2nd, 3rd, 4th, South Dakota
2017: Beijing
2018: Portugal
2019: Moscow, online
2020: didn't make it, Dhaka
2021: Luxor, Egypt, in Apr
2022: Luxor, Egypt, in Apr
2023: Astana, Kazakhstan in Sep
*/

/*
UTCC
    chess
*/

/*
memorial
smithson
prosper
coppell
college station
plano west
midway
*/
