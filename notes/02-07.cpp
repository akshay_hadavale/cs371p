// -----------
// Wed,  7 Feb
// -----------

/*
Reminders
    Sun, 11 Feb: Blog  #4
    Sun, 11 Feb: Paper #4: Pair Programming

    UT Programming Club (UTPC)
    Contest #2
    F, 9 Feb, 5:30 pm, GDC 2.216
*/

const char* s = "abc";
const char* t = "abc";
assert(s == t);        // identity
assert(strcmp(s, st)); // content

string s = "abc";
string t = "abc";
assert(&s == &t); // identity
assert(s  == t);  // content





int my_strcmp (const char* a, const char* b) {
    while ((*a != 0) && (*a == *b)) {
        ++a;
        ++b;}
    return (*a - *b);}





#ifdef TEST0
void test0 () {
    assert(my_strcmp("",    "")    == 0);}
#endif

#ifdef TEST1
void test1 () {
    assert(my_strcmp("abc", "abc") == 0);}
#endif

#ifdef TEST2
void test2 () {
    assert(my_strcmp("abc", "ab")  >  0);}
#endif

#ifdef TEST3
void test3 () {
    assert(my_strcmp("abc", "aba") >  0);}
#endif

#ifdef TEST4
void test4 () {
    assert(my_strcmp("ab",  "abc") <  0);}
#endif

#ifdef TEST5
void test5 () {
    assert(my_strcmp("aba", "abc") <  0);}
#endif





/*
values
pointers
references
*/

void f (int v) {
    ++v;}

int i = 2;
f(v);
cout << i; // 2





void g (int* p) {
    ++p;          // no
    ++*p;}

int j = 3;
g(j);      // compile-time error
g(&j);
cout << j; // 4

g(185); // compile-time error
g(0);   // runtime error





void h (int& r) { // must be an l-value
    ++*r;         // compile-time error
    ++r;}

int k = 4;
h(&k);     // compile-time error
h(k);
cout << k; // 5

h(185); // compile-time error
h(0);   // compile-time error





/*
Independence, ford vs ferrari

Centennial, tae kwon do

LASA, soccer

Flower Mound, tennis

Clear Creek, brazilian jiu jitsu

Belkin, running
*/
