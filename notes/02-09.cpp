// -----------
// Fri,  9 Feb
// -----------

/*
Reminders
    Sun, 11 Feb: Blog  #4
    Sun, 11 Feb: Paper #4: Pair Programming

    UT Programming Club (UTPC)
    Contest #2 (individual)
    F, 9 Feb, 5:30 pm, GDC 2.216
*/

int i; // doesn't have to be initialized
i = 2;

const int ci;     // no
const int ci = 2; // has to be initialized
ci = 3;           // no





// read/write, many-location

int i = 2;
int j = 3;
const int ci = 4;
const int cj = 5;

int* p; // doesn't have to be initialized
p = i;  // no
p = &i;
p = &j;

*p = 5;
p = &ci; // no





// read-only, many-location

int i = 2;
int j = 3;
const int ci = 4;
const int cj = 5;

const int* pc; // doesn't have to be initialized
pc = &ci;
pc = &cj;

*pc = 6;       // no
pc = &i;       // ok!!!





// read/write, one-location

int i = 2;
int j = 3;
const int ci = 4;
const int cj = 5;

int* const pc;       // no
int* const pc = &ci; // no
int* const pc = &i;  // has to be initialized

*pc = 6;
pc = &j;             // no

int* a = new int[100];
++*a;                  // yes
++a;                   // no; declaring int* const a, would have made this line not compile
...
delete [] a;           // ++a, above, makes this undefined





// read-only, one-location

int i = 2;
int j = 3;
const int ci = 4;
const int cj = 5;

const int* const cpc1;       // no
const int* const cpc1 = &ci; // has to be initialized
const int* const cpc2 = &i;

++*cpc1;    // no
cpc1 = &cj; // no





// read-write, one-location

int i = 2;
int j = 3;
const int ci = 4;
const int cj = 5;

int& r;       // no
int& r = &ci; // no
int& r = ci;  // no
int& r = i;   // has to be initialized
r = 6;





// read-only, one-location

int i = 2;
int j = 3;
const int ci = 4;
const int cj = 5;

const int& cr;      // no
const int& cr = ci; // has to be initialized
cr = 6;             // no

const int& cr2 = i;





T*
const T*
T* const       // most like T&
const T* const // most like const T&





/*
Eastwood, cooking

Mansfield Legacy, Nope

Sulfur Springs, this is how you a time war

Ronald Reagan, basketball

Westwood, guitar

Academy, crossword

Katy Taylor, chess
*/
