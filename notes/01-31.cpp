// -----------
// Wed, 31 Jan
// -----------

/*
Reminders
    Sun, 4 Feb: Blog  #3
    Sun, 4 Feb: Paper #3: Continuous Integration

    UT Programming Club (UTPC)
    Contest #1
    F, 9 Feb, 5:30 pm, GDC 2.216
*/

/*
Voting
*/

// sample input
3

3
John Doe
Jane Smith
Sirhan Sirhan
1 2 3
2 1 3
1 3 2
1 2 3

3
John Doe
Jane Smith
Sirhan Sirhan
1 2 3
2 1 3
2 3 1
1 2 3

3
John Doe
Jane Smith
Sirhan Sirhan
1 2 3
2 1 3
2 3 1
1 2 3
3 1 2

// sample output

John Doe

John Doe
Jane Smith

John Doe





/*
count the number of 1st preferences
50% or more for any one, that's the winner, we print the winner

if there's a tie of all the candidates, then we're done, we print all of them

keep going
*/





/*
code
checktestdata schema
*/





/*
1. one bucket of ballots (e.g., vector of vector of int)

2. one bucket of ballots per candidate

3. one bucket of ballots
   one bucket of ballot pointers per candidate
*/





/*
San Marcos, legos

Godley, volleyball

Cypress Ridge, badminton
*/
