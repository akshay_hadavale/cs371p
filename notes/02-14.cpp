// -----------
// Wed, 14 Feb
// -----------

/*
Reminders
    Sun, 18 Feb: Blog  #5
    Sun, 18 Feb: Paper #5: The Single Responsibility Principle

    UT Programming Club (UTPC)
    Contest #3 (team)
    F, 1 Mar, 5:30 pm, GDC 2.216
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/

// -----------
// Arrays1.cpp
// -----------

// https://en.cppreference.com/w/cpp/container/array

#include <cstdio>

#include <algorithm> // copy, equal
#include <array>     // array
#include <cassert>   // assert
#include <iostream>  // cout, endl

using namespace std;

void test3 () {
    const size_t s = 10;
//  const int    a[s];       // error: uninitialized const 'a'
    int a[s];                // O(1)
    assert(sizeof(a) == 40);
//  assert(a[0]      ==  0); // undefined

    T a[s];                  // O(n)
    }

void test4 () {
    const size_t s    = 10;
    const int    a[s] = {};   // O(n)
    assert(sizeof(a) == 40);
    assert(a[0]      ==  0);
    const T a[s] = {};        // O(n)
    }





// Java

class A
    {}

A x = new A();
A y = new A();

x == y; // identity, false

x.equals(y); // content, false





class A {
    bool equals (A rhs) {
        return true;}}

A x = new A();
A y = new A();

x == y; // identity, false

x.equals(y); // content, true





bool equal (int* b1, int* e1, int* b2) {
    while (b1 != e1) {
        if (*b1 != *b2)
            return false;
        ++b1;
        ++b2;}
    return true;}

int a[] = ...
int b[] = ...
equal(a + 5, a + 10, b + 15); // what's the min size of b? 20

int a[] = ...
long b[] = ...
equal(a + 5, a + 10, b + 15); // no





bool equal (int* b1, int* e1, long* b2) {
    while (b1 != e1) {
        if (*b1 != *b2)
            return false;
        ++b1;
        ++b2;}
    return true;}

int a[] = ...
int b[] = ...
equal(a + 5, a + 10, b + 15); // no

int a[] = ...
long b[] = ...
equal(a + 5, a + 10, b + 15); // yes





template <typename T1, typename T2>
bool equal (T1* b1, T1* e1, T2* b2) {
    while (b1 != e1) {
        if (*b1 != *b2)
            return false;
        ++b1;
        ++b2;}
    return true;}

int a[] = ...
int b[] = ...
equal(a + 5, a + 10, b + 15); // yes
T1 -> int
T2 -> int

int a[] = ...
long b[] = ...
equal(a + 5, a + 10, b + 15); // yes
T1 -> int
T2 -> long

list<int>  a = {2, 3, 4}
list<long> b = {3, 4, 5}
equal(a.begin(), a.end(), b.begin()) // no






template <typename T1, typename T2>
bool equal (T1 b1, T1 e1, T2 b2) {
    while (b1 != e1) {
        if (*b1 != *b2)
            return false;
        ++b1;
        ++b2;}
    return true;}

int a[] = ...
int b[] = ...
equal(a + 5, a + 10, b + 15); // yes
T1 -> int*
T2 -> int*

int a[] = ...
long b[] = ...
equal(a + 5, a + 10, b + 15); // yes
T1 -> int*
T2 -> long*

list<int>  a = {2, 3, 4}
list<long> b = {3, 4, 5}
equal(a.begin(), a.end(), b.begin()) // yes
T1 -> list<int>::iterator
T2 -> list<long>::iterator





template <typename T1, typename T2>
bool equal (T1 b1, T1 e1, T2 b2) {
    while (b1 != e1) {
        if (*b1 != *b2)
            return false;
        ++b1;
        ++b2;}
    return true;}

/*
T1: !=, *, ++
T2: *, ++
*/





/*
5 types of iterator

input
    ==, !=, * (read only), ++
output
    * (write only), ++
forward
    ==, !=, * (read write), ++
bidirectionsl
    ==, !=, * (read write), ++, --
random access
    ==, !=, * (read write), ++, --, [], +, -, <, >, <=, >=
*/





// Java

LinkedList<Integer> x = new LinkedList<Integer>();
Iterator<Integer>   p = x.iterator();
while (p.hasNext()) {
    ...p.next()...}





list<int>  a = {2, 3, 4}
list<long> b = {3, 4, 5}
list<int>::iterator  b1 = a.begin();
cout << *b1; // 2
cout << *b1; // 2
++b1;
cout << *b1; // 3

list<int>::iterator  e1 = a.end();
list<long>::iterator b2 = b.begin();





void test5 () {
    int a[] = {2, 3, 4};
    int b[] = {2, 3, 4};
    assert(a != b);             // identity check
    assert(equal(a, a + 3, b));
    ++b[1];
    assert(equal(a, a + 3, begin({2, 3, 4})));
    assert(equal(b, b + 3, begin({2, 4, 4})));}





/*
Halo

The Boys

boeing

the expanse

haikyu

percy jackson

perfect blue
*/
