// -----------
// Fri, 26 Jan
// -----------

/*
Reminders
    Sun, 28 Jan: Blog  #2
    Sun, 28 Jan: Paper #2: Makefile

    UT Programming Club (UTPC)
    Contest #1
    F, 26 Jan, 5:30 pm, GDC 2.216
*/

/*
pre-increment takes an l-value
returns an l-value
*/

int i = 2;
int j = ++i;
cout << i; // 3
cout << j; // 3

++++i;





int i = 2;
int j = i++;
cout << i; // 3
cout << j; // 2

/*
post-increment takes an l-value
returns an r-value
*/

i++++; // no





// int's <<
// takes two r-values
// returns an r-value
int i = 2;
int j = 3;
int k = (i << j); // built-in type operator





// ostream's <<
// takes an l-value and a r-value
// returns l-value
// cout is an instance of the type ostream
// ostream has overloaded the << operator
cout << "hi"; // user-defined type operator
cout << "hi" << "bob";
cout << "hi" returns cout





// unaries
++, --, -

// binaries
==, +, -, *, /, %

// ternary
? :

/*
overloading operators
    can I invent a new token, like ,,,; no
    can I change the precedence? no
    can I change the associativity? no
    can I change the number of arguments? no
    can I change the l-value / r-value nature? yes
    can I define an operator on a built-in type that is not defined? no
*/





// --------------
// UnitTests1.cpp
// --------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 0;
    while (n > 1) {
        if ((n % 2) == 0)
            n /= 2;
        else
            ++(n *= 3);
        ++c;}
    assert(c > 0);
    return c;}

TEST(UnitTestsFixture, test0) {
    ASSERT_EQ(cycle_length( 1), 1);}

TEST(UnitTestsFixture, test1) {
    ASSERT_EQ(cycle_length( 5), 6);}

TEST(UnitTestsFixture, test2) {
    ASSERT_EQ(cycle_length(10), 7);}

/*
% clang++ --coverage -g -std=c++20 -Wall -Wextra -Wpedantic UnitTests1.cpp -o UnitTests1 -lgtest -lgtest_main



% UnitTests1
Running main() from /tmp/googletest-20230802-5079-wamwr8/googletest-1.14.0/googletest/src/gtest_main.cc
[==========] Running 3 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 3 tests from UnitTestsFixture
[ RUN      ] UnitTestsFixture.test0
Assertion failed: (c > 0), function cycle_length, file UnitTests1.cpp, line 22.
*/





// --------------
// UnitTests2.cpp
// --------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n /= 2;
        else
            ++(n *= 3);
        ++c;}
    assert(c > 0);
    return c;}

TEST(UnitTestsFixture, test0) {
    ASSERT_EQ(cycle_length( 1), 1);}

TEST(UnitTestsFixture, test1) {
    ASSERT_EQ(cycle_length( 5), 5);}

TEST(UnitTestsFixture, test2) {
    ASSERT_EQ(cycle_length(10), 7);}

/*
% ./UnitTests2
Running main() from /tmp/googletest-20210612-68642-1k2ka09/googletest-release-1.12.1/googletest/src/gtest_main.cc
[==========] Running 3 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 3 tests from UnitTestsFixture
[ RUN      ] UnitTestsFixture.test0
[       OK ] UnitTestsFixture.test0 (0 ms)
[ RUN      ] UnitTestsFixture.test1
UnitTests2.cpp:29: Failure
Expected equality of these values:
  cycle_length( 5)
    Which is: 6
  5
[  FAILED  ] UnitTestsFixture.test1 (0 ms)
[ RUN      ] UnitTestsFixture.test2
[       OK ] UnitTestsFixture.test2 (0 ms)
[----------] 3 tests from UnitTestsFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 3 tests from 1 test suite ran. (0 ms total)
[  PASSED  ] 2 tests.
[  FAILED  ] 1 test, listed below:
[  FAILED  ] UnitTestsFixture.test1

 1 FAILED TEST
*/





// --------------
// UnitTests3.cpp
// --------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n /= 2;
        else
            ++(n *= 3);
        ++c;}
    assert(c > 0);
    return c;}

TEST(UnitTestsFixture, test1) {
    ASSERT_EQ(cycle_length( 1), 1);}

TEST(UnitTestsFixture, test2) {
    ASSERT_EQ(cycle_length( 5), 6);}

TEST(UnitTestsFixture, test3) {
    ASSERT_EQ(cycle_length(10), 7);}

/*
% ./UnitTests3
Running main() from /tmp/googletest-20210612-68642-1k2ka09/googletest-release-1.12.1/googletest/src/gtest_main.cc
[==========] Running 3 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 3 tests from UnitTestsFixture
[ RUN      ] UnitTestsFixture.test1
[       OK ] UnitTestsFixture.test1 (0 ms)
[ RUN      ] UnitTestsFixture.test2
[       OK ] UnitTestsFixture.test2 (0 ms)
[ RUN      ] UnitTestsFixture.test3
[       OK ] UnitTestsFixture.test3 (0 ms)
[----------] 3 tests from UnitTestsFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 3 tests from 1 test suite ran. (0 ms total)
[  PASSED  ] 3 tests.
*/





// -------------
// Coverage1.cpp
// -------------

// https://gcc.gnu.org/onlinedocs/gcc/Gcov.html

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n /= 2;
        else
            ++(n *= 3);
        ++c;}
    assert(c > 0);
    return c;}

TEST(CoverageFixture, test) {
    ASSERT_EQ(cycle_length(1), 1);}

/*
% clang++ --coverage -g -std=c++17 -Wall -Wextra -Wpedantic Coverage1.cpp -o Coverage1



% ./Coverage1
Running main() from /tmp/googletest-20210612-68642-1k2ka09/googletest-release-1.12.1/googletest/src/gtest_main.cc
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from CoverageFixture
[ RUN      ] CoverageFixture.test
[       OK ] CoverageFixture.test (0 ms)
[----------] 1 test from CoverageFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (0 ms total)
[  PASSED  ] 1 test.



% ls -al Coverage1.*
-rw-r--r--@ 1 downing  staff    1558 Sep  4 14:44 Coverage1.cpp
-rw-r--r--@ 1 downing  staff    2678 Mar  1 16:09 Coverage1.cpp.gcov
-rw-r--r--@ 1 downing  staff    8764 Mar  1 16:09 Coverage1.gcda
-rw-r--r--@ 1 downing  staff  131168 Mar  1 16:09 Coverage1.gcno



% llvm-cov gcov Coverage1.cpp | grep -B 2 "cpp.gcov"
File 'Coverage1.cpp'
Lines executed:66.67% of 12
Creating 'Coverage1.cpp.gcov'
*/





// -------------
// Coverage2.cpp
// -------------

// https://gcc.gnu.org/onlinedocs/gcc/Gcov.html

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n /= 2;
        else
            ++(n *= 3);
        ++c;}
    assert(c > 0);
    return c;}

TEST(CoverageFixture, test) {
    ASSERT_EQ(cycle_length(2), 2);}

/*
% ./Coverage2
Running main() from /tmp/googletest-20210612-68642-1k2ka09/googletest-release-1.12.1/googletest/src/gtest_main.cc
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from CoverageFixture
[ RUN      ] CoverageFixture.test
[       OK ] CoverageFixture.test (0 ms)
[----------] 1 test from CoverageFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (0 ms total)
[  PASSED  ] 1 test.



% llvm-cov gcov Coverage2.cpp | grep -B 2 "cpp.gcov"
File 'Coverage2.cpp'
Lines executed:91.67% of 12
Creating 'Coverage2.cpp.gcov'
*/





// -------------
// Coverage3.cpp
// -------------

// https://gcc.gnu.org/onlinedocs/gcc/Gcov.html

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n /= 2;
        else
            ++(n *= 3);
        ++c;}
    assert(c > 0);
    return c;}

TEST(CoverageFixture, test) {
    ASSERT_EQ(cycle_length(3), 8);}

/*
% ./Coverage3
Running main() from /tmp/googletest-20210612-68642-1k2ka09/googletest-release-1.12.1/googletest/src/gtest_main.cc
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from CoverageFixture
[ RUN      ] CoverageFixture.test
[       OK ] CoverageFixture.test (0 ms)
[----------] 1 test from CoverageFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (0 ms total)
[  PASSED  ] 1 test.



% llvm-cov gcov Coverage3.cpp | grep -B 2 "cpp.gcov"
File 'Coverage3.cpp'
Lines executed:100.00% of 12
Creating 'Coverage3.cpp.gcov'
*/





/*
motivate exceptions
pretend that I don't have them
    C
*/





// make use of the return

int f (...) {
    ...
    if (<something wrong>)
        return <special value>
    ...}

int g (...) {
    ...
    int i = f(...);
    if (i = <special value>)
        <something wrong>
    ...}





// make use of a global, doesn't link, no definition of h

// foo.cpp
extern int h;

int f (...) {
    ...
    if (<something wrong>) {
        h = <special value>;
        return ...;
    ...}

// bar.cpp
extern int h;

int g (...) {
    ...
    h = 0;
    int i = f(...);
    if (h == <special value>)
        <something wrong>
    ...}





// make use of a global, doesn't link, two definitions of h

// foo.cpp
int h;

int f (...) {
    ...
    if (<something wrong>) {
        h = <special value>;
        return ...;
    ...}

// bar.cpp
int h;

int g (...) {
    ...
    h = 0;
    int i = f(...);
    if (h == <special value>)
        <something wrong>
    ...}





// make use of a global

// foo.cpp
extern int h;

int f (...) {
    ...
    if (<something wrong>) {
        h = <special value>;
        return ...;
    ...}

// bar.cpp
int h;

int g (...) {
    ...
    h = 0;
    int i = f(...);
    if (h == <special value>)
        <something wrong>
    ...}

// make use of a parameter
// pass by value doesn't work

int f (..., int e2) {
    ...
    if (<something wrong>) {
        e2 = <special value>
        return ...;
    ...}

int g (...) {
    ...
    int e = 0;
    int i = f(..., e);
    if (e == <special value>)
        <something wrong>
    ...}


// make use of a parameter
// pass by address

int f (..., int* e2) {
    assert(e2);
    ...
    if (<something wrong>) {
        *e2 = <special value>
        return ...;
    ...}

int g (...) {
    ...
    int e = 0;
    int i = f(..., &e);
    if (e == <special value>)
        <something wrong>
    ...}




// make use of a parameter
// pass by reference

int f (..., int& e2) { // demands an l-value
    ...
    if (<something wrong>) {
        e2 = <special value>
        return ...;
    ...}

int g (...) {
    ...
    int e = 0;
    int i = f(..., e);
    if (e == <special value>)
        <something wrong>
    ...}



/*
Clements, badminton

South San Antonio, basketball

Frisco, airplane

Nederland, bake

Prosper, suits

Klein Forest, london model, gundam

Carnegie Vanguard, mechanical keyboards

Coppel, politics

Hebbron, Bollywood,

Clark, basketball
*/
