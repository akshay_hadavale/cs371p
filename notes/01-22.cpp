// -----------
// Mon, 22 Jan
// -----------

/*
Reminders
    Sun, 28 Jan: Blog  #2
    Sun, 28 Jan: Paper #2: Makefile

    UT Programming Club (UTPC)
    Beginners' Workshop
    Th, 25 Jan, 6 pm, GDC auditorium

    UT Programming Club (UTPC)
    Contest #1
    F, 26 Jan, 5:30 pm, GDC auditorium
*/

/*
tools
    astyle
    checktestdata
    doxygen
    gcov
    Google Test
*/



/*
1. ssh CS
2. install the tools on our local machines
3. Docker
*/



/*
# -----------------
# Create-Docker.txt
# -----------------

# https://www.docker.com
# https://docs.docker.com/engine/reference/run/



% which docker
/usr/local/bin/docker



% docker --version
Docker version 24.0.5, build ced0996600



% docker images
REPOSITORY   TAG       IMAGE ID   CREATED   SIZE



% cat Dockerfile
FROM gcc

# update Linux package manager
RUN apt-get update

# auto formatter
RUN apt-get -y install astyle

# static analyzer
RUN apt-get -y install cppcheck

# configuration tool
RUN apt-get -y install cmake

# line-ending converter
RUN apt-get -y install dos2unix

# documentation generator
RUN apt-get -y install doxygen

# graph visualization, required by GMP
RUN apt-get -y install graphviz

# memory checker
RUN apt-get -y install valgrind

# editor
RUN apt-get -y install vim

# Boost library, required by checktestdata
RUN apt-get -y install libboost-dev
RUN apt-get -y install libboost-serialization-dev

# GNU bignum library, required by checktestdata
RUN apt-get -y install libgmp-dev

# Google Test
RUN apt-get -y install libgtest-dev

# build checktestdata, an input verifier
RUN git clone https://github.com/DOMjudge/checktestdata checktestdata && \
    cd checktestdata                                                  && \
    git checkout release                                              && \
    ./bootstrap                                                       && \
    make                                                              && \
    cp checktestdata /usr/bin                                         && \
    cd -

# build Google Test
RUN cd /usr/src/gtest                                                 && \
    cmake CMakeLists.txt                                              && \
    make                                                              && \
    cp lib/*.a /usr/lib                                               && \
    cd -

# Bash shell
CMD bash



# -t: Name and optionally a tag in the 'name:tag' format
% docker build -t gpdowning/gcc .
...



% docker images
REPOSITORY      TAG       IMAGE ID       CREATED         SIZE
gpdowning/gcc   latest    3007475ae5a2   2 minutes ago   2.17GB



% docker login -u gpdowning
Password: <access token>
...



% docker push gpdowning/gcc
...



# https://hub.docker.com/r/gpdowning/gcc/

*/



/*
# --------------
# Use-Docker.txt
# --------------

# https://www.docker.com
# https://docs.docker.com/engine/reference/run/



% which docker
/usr/local/bin/docker



% docker --version
Docker version 24.0.5, build ced0996600



% docker images
REPOSITORY   TAG       IMAGE ID   CREATED   SIZE



% docker pull gpdowning/gcc
...



% docker images
REPOSITORY      TAG       IMAGE ID       CREATED          SIZE
gpdowning/gcc   latest    3007475ae5a2   49 minutes ago   2.17GB



% pwd
/Users/downing/git/cs371p/cpp



% ls
Create-Docker.txt    Dockerfile        Makefile        Use-Docker.txt



# --rm: Automatically remove the container when it exits
# -i:   Keep STDIN open even if not attached
# -t:   Allocate a pseudo-TTY
# -v:   Bind mount a volume
# -w:   Working directory inside the container
% docker run --rm -i -t -v /Users/downing/git/cs371p/cpp:/usr/gcc -w /usr/gcc gpdowning/gcc
/usr/gcc# pwd
/usr/gcc



/usr/gcc# ls
Create-Docker.txt  Dockerfile  Makefile  Use-Docker.txt



/usr/gcc# which astyle
/usr/bin/astyle

/usr/gcc# astyle --version
Artistic Style Version 3.1



/usr/gcc# which checktestdata
/usr/bin/checktestdata

/usr/gcc# checktestdata --version
checktestdata -- version 20230822, written by Jan Kuipers, Jaap Eldering, Tobias Werth



/usr/gcc# which cppcheck
/usr/bin/cppcheck

/usr/gcc# cppcheck --version
Cppcheck 2.10



/usr/gcc# which doxygen
/usr/bin/doxygen
/usr/gcc# doxygen --version
1.9.4



# which g++
/usr/local/bin/g++

/usr/gcc# g++ --version
g++ (GCC) 13.2.0



/usr/gcc# which valgrind
/usr/bin/valgrind

/usr/gcc# valgrind --version
valgrind-3.19.0



/usr/gcc# which vim
/usr/bin/vim

/usr/gcc# vim --version
VIM - Vi IMproved 9.0 (2022 Jun 28, compiled May 04 2023 10:24:44)



/usr/gcc# grep "#define BOOST_LIB_VERSION " /usr/include/boost/version.hpp
#define BOOST_LIB_VERSION "1_74"



/usr/gcc# ls -al /usr/include/gtest/gtest.h
-rw-r--r-- 1 root root 89715 Jun 27  2022 /usr/include/gtest/gtest.h



/usr/gcc# ls -al /usr/lib/libgtest*.a
-rw-r--r-- 1 root root 2493090 Aug 22 16:36 /usr/lib/libgtest.a
-rw-r--r-- 1 root root    3004 Aug 22 16:36 /usr/lib/libgtest_main.a



/usr/gcc# pkg-config --modversion gtest
1.12.1
*/



/*
I-School, Starcraft II

Tompkins, tennis

Fossil Ridge, sim racing, asseto corsa

Plano West, badminton

Ray, sport

Reedy, table tennis

Mansfield Legacy, baby jumping spider

Liberty, kayaking

Prosper, making videos
*/
