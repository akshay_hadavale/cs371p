// -----------
// Fri,  8 Mar
// -----------

/*
Reminders
    Sun, 10 Mar: Blog         #8
    Sun, 10 Mar: Paper        #8. The Integration Segregation Principle
    Fri, 22 Mar: UTPC Contest #4 (individual)
    Wed,  3 Apr: Project      #4
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/

/*
5 types of iterator

input
    ==, !=, * (read only), ++
output
    * (write only), ++
forward
    ==, !=, * (read/write), ++
bidirectional
    ==, !=, * (read/write), ++, --
random access
    ==, !=, * (read/write), ++, --, [], +, -, <, <=, >, >=
*/

/*
Last Class
    arrays on the heap
    std::unique_ptr
    std::vector

This Class
*/

// -----------
// Arrays2.cpp
// -----------

/*
things we can do wrong with delete
1. delete wrong address
2. didn't delete, memory leak
3. delete more than once
4. delete too early, dereference pointer after deletion
*/

void test7 () {
    const ptrdiff_t   s = 10;
    const int         v =  2;
    unique_ptr<int[]> a(new int[s]);
    fill(a.get(), a.get() + s, v);
    assert(count(a.get(), a.get() + s, v) == s);
    assert(a.get()[1] == v);
    f(a.get());
    assert(a.get()[1] == v + 2);}

void test8 () {
    const size_t s = 10;
    const int    v =  2;
    vector<int>  x(s, v);    // T(T), s times
    assert(x.size() == s);
    assert(x[0]     == v);
    vector<int> y(x);        // T(T), s times
    assert( x    ==  y);     // ==(T), s times
    assert(&x[1] != &y[1]);
    vector<int> z(2 * s, v); // T(T), 2*s times
    x = z;                   // ~T(), s times; T(T), 2*s times
    assert( x    ==  z);
    assert(&x[1] != &z[1]);}

void h1 (vector<int>& y) {
    vector<int>::iterator p = begin(y);
    ++p;
    ++p[0];
    ++*p;}

void test9 () {
    vector<int> x = {2, 3, 4};
    h1(x);
    assert(equal(begin(x), end(x), begin({2, 5, 4})));}

void h2 (vector<int> y) {
    vector<int>::iterator p = begin(y);
    ++p;
    ++p[0];
    ++*p;}

void test10 () {
    vector<int> x = {2, 3, 4};
    h2(x);
    assert(equal(begin(x), end(x), begin({2, 3, 4})));}





/*
containers
*/

/*
vector like ArrayList
front-loaded array
cost of adding to the front:      O(n)
cost of adding to the middle:     O(n)
cost of adding to the back:       amortized O(1)
cost of removing from the front:  O(n)
cost of removing from the middle: O(n)
cost of removing from the back:   O(1)
cost of indexing:                 O(1)
*/

vector<int> x;
cout << x.size();     // 0
cout << x.capacity(); // 0
x.push_back(2);
cout << x.size();     // 1
cout << x.capacity(); // 1
x.push_back(3);
cout << x.size();     // 2
cout << x.capacity(); // 2
x.push_back(4);
cout << x.size();     // 3
cout << x.capacity(); // 4





vector<int> x(10, 2)
cout << x.size();     // 10
cout << x.capacity(); // 10
x.push_back(4);
cout << x.size();     // 11
cout << x.capacity(); // 20





vector<int> x(10, 2);
int* p = &x[5];
cout << p;            // an address
cout << *p;           // 2
x.push_back(3);
cout << p;            // an address
cout << *p;           // undefined





/*
the wire

the beekeeper

jack ryan

suits

whiplash

manifest

kung fu panda
*/
