// -----------
// Mon, 18 Mar
// -----------

/*
Reminders
    Fri, 22 Mar: UTPC Contest #4 (individual)
    Sun, 24 Mar: Blog         #9
    Sun, 24 Mar: Paper        #9. The Dependency Inversion Principle
    Wed,  3 Apr: Project      #4:
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/

/*
5 types of iterator

input
    ==, !=, * (read only), ++
output
    * (write only), ++
forward
    ==, !=, * (read/write), ++
bidirectional
    ==, !=, * (read/write), ++, --
random access
    ==, !=, * (read/write), ++, --, [], +, -, <, <=, >, >=
*/

/*
Last Class
    arrays on the heap
    std::unique_ptr
    std::vector

This Class
*/

using array_t = array<size_t, 5>;

class A {
    friend bool operator == (const A& lhs, const A& rhs) {
        return lhs._v == rhs._v;}

    public:
        static array_t c;

        static void reset () {
            c = array_t({0, 0, 0, 0, 0});}

    private:
        int _v;

    public:
        A () :                         // default constructor
            _v (0) {
            ++c[0];}

        A (int v) :                    // int constructor
            _v (v) {
            ++c[1];}

        A (const A& rhs) :             // copy constructor
            _v (rhs._v) {
            ++c[2];}

        A& operator = (const A& rhs) { // copy assignment
            _v = rhs._v;
            ++c[3];
            return *this;}

        ~A () {                        // destructor
            ++c[4];}};

array_t A::c;

using vector_t = my_vector<A>;

#ifdef TEST1
void test1 () {
    A::reset();
    vector_t x; // def constr
    assert(A::c == array_t({0, 0, 0, 0, 0}));
    assert(x    == vector_t({}));} // ==, il constr
#endif

#ifdef TEST2
void test2 () {
    A::reset();
    vector_t x(3); // int constr
    assert(A::c == array_t({4, 0, 0, 3, 1})); // 4 A(), 3 =(A), ~A()
    assert(x    == vector_t({0, 0, 0}));}
#endif

#ifdef TEST3
void test3 () {
    A::reset();
    const vector_t x(3, 2);
    assert(A::c == array_t({3, 1, 0, 3, 1})); // 3 A(), A(int), 3 =(A), ~A()
    assert(x    == vector_t({2, 2, 2}));}
#endif

#ifdef TEST4
void test4 () {
    A::reset();
    const initializer_list<A> x = {2, 3, 4};
    assert(A::c == array_t({0, 3, 0, 0, 0})); // 3 A(int)
    assert(x    == vector_t({2, 3, 4}));

    A::reset();
    const vector_t y(x);
    assert(A::c == array_t({3, 0, 0, 3, 0})); // 3 A(), 3 =(A)
    assert(y    == vector_t({2, 3, 4}));}
#endif

#ifdef TEST5
void test5 () {
    vector_t x = {2, 3, 4};
    assert(x[1] == 3);
    x[1] = 5;
    assert(x == vector_t({2, 5, 4}));}
#endif

#ifdef TEST6
void test6 () {
    const vector_t x = {2, 3, 4};
    assert(x[1] == 3);
//  x[1] = 5;                     // error: cannot assign to return value because function 'operator[]' returns a const value
    }
#endif





/*
michael jordan's doc

youtube
*/
