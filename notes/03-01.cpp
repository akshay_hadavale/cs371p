// -----------
// Fri,  1 Mar
// -----------

/*
Reminders
    Fri,  1 Mar: UTPC Contest #3 (team)
    Sun,  3 Mar: Blog         #7
    Sun,  3 Mar: Paper        #7: The Liskov Substitution Principle
    Wed,  6 Mar: Project      #3: Allocator
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/

/*
5 types of iterator

input
    ==, !=, * (read only), ++
output
    * (write only), ++
forward
    ==, !=, * (read/write), ++
bidirectional
    ==, !=, * (read/write), ++, --
random access
    ==, !=, * (read/write), ++, --, [], +, -, <, <=, >, >=
*/

/*
Last Class
    operator ==

This Class
    arrays
    std::array
*/

// -----------
// Arrays1.cpp
// -----------

void test6 () {
    int a[] = {2, 3, 4};
//  int b[] = a;                    // error: initializer fails to determine size of 'b'
    int* b  = a;
    assert(a         == b);
    assert(sizeof(a) != sizeof(b));
    assert(sizeof(a) == 12);
    assert(sizeof(b) ==  8);
    ++b[1];
    assert(equal(a, a + 3, begin({2, 4, 4})));
    assert(equal(b, b + 3, begin({2, 4, 4})));}

void test7 () {
    int          a[] = {2, 3, 4};
    const size_t s   = sizeof(a) / sizeof(a[0]);
    int b[s];
    copy(a, a + s, b);
    assert(a != b);
    assert(equal(a, a + s, b));
    ++b[1];
    assert(equal(a, a + 3, begin({2, 3, 4})));
    assert(equal(b, b + 3, begin({2, 4, 4})));}





T x = ...;
T y = x;   // T(T)
x = y;     // =(T)

T a[];         // no
T a[s];        // O(1) or O(n), T()
T a[] = {...}; // O(n)

T a[s];            // O(n), T()
fill(a, a + s, v); // O(n), =(T)

std:array<T>     x; // no
std::array<T, s> x; // O(n), T()





void test8 () {
    int a[] = {2, 3, 4};
    int b[] = {5, 6, 7};
//  b = a;                                     // error: invalid array assignment
    const size_t s = sizeof(a) / sizeof(a[0]);
    copy(a, a + s, b);
    assert(a != b);
    assert(equal(a, a + s, b));
    ++b[1];
    assert(equal(a, a + 3, begin({2, 3, 4})));
    assert(equal(b, b + 3, begin({2, 4, 4})));}

void f (int p[]) {
    assert(sizeof(p) == 8); // warning: sizeof on array function parameter will return size of 'int *' instead of 'int []'
    ++p;
    ++p[0];
    ++*p;}

void test9 () {
    int a[] = {2, 3, 4};
    f(a);
    assert(equal(a, a + 3, begin({2, 5, 4})));}

void g (int* p) {
    assert(sizeof(p) == 8);
    ++p;
    ++p[0];
    ++*p;}

void test10 () {
    int a[] = {2, 3, 4};
    g(a);
    assert(equal(a, a + 3, begin({2, 5, 4})));}





/*
built-in arrays
don't compare
don't know their size
don't initialize
don't copy
don't assign
only pass by address
*/





struct A {
    B x;

    A ()                  = default; // default constructor
    A (const A&)          = default; // copy    constructor
    ~A ()                 = default; // destructor
    operator = (const A&) = default; // copy    assignment
};





void test11 () {
    array<int, 3> x = {2, 3, 4};
    assert(x.size() == 3);
    assert(x[1]     == 3);
    array<int, 3> y(x);          // T(T),  s times
    assert( x    ==  y);         // ==(T), s times
    assert(&x[1] != &y[1]);}

void test12 () {
    array<int, 3> x = {2, 3, 4};
    array<int, 3> y = {5, 6, 7};
    x = y;                       // =(T), s times
    assert( x    ==  y);
    assert(&x[1] != &y[1]);}

void h1 (array<int, 3>& y) {
    array<int, 3>::iterator p = begin(y);
    ++p;
    ++p[0];
    ++*p;}

void test13 () {
    array<int, 3> x = {2, 3, 4};
    h1(x);
    assert(equal(begin(x), end(x), begin({2, 5, 4})));}

void h2 (array<int, 3> y) {
    array<int, 3>::iterator p = begin(y);
    ++p;
    ++p[0];
    ++*p;}

void test14 () {
    array<int, 3> x = {2, 3, 4};
    h2(x);
    assert(equal(begin(x), end(x), begin({2, 3, 4})));}





/*
the bear

eric andre

waywood pines

mindcraft
*/
