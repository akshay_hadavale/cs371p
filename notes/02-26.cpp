// -----------
// Mon, 26 Feb
// -----------

/*
Reminders
    Fri,  1 Mar: UTPC Contest #3 (team)
    Sun,  3 Mar: Blog         #7
    Sun,  3 Mar: Paper        #7: The Liskov Substitution Principle
    Wed,  6 Mar: Project      #3: Allocator
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/

/*
5 types of iterator

input
    ==, !=, * (read only), ++
output
    * (write only), ++
forward
    ==, !=, * (read/write), ++
bidirectional
    ==, !=, * (read/write), ++, --
random access
    ==, !=, * (read/write), ++, --, [], +, -, <, <=, >, >=
*/

/*
Last Class
    Exercise #4: Digits

This Class
    Exercise #4: Digits
*/

struct A {
	int _v;

	A (int v) :
		_v (v)

	int& f ()  {
		// ++_v;            // would be legal
		// g();             // would be legal
		return _v;}

	const int& f () const {
		++_v;               // no
		g();                // no
		return _v;}

	int g () {
		++_v;
		f();}

A x(2);
cout << x.f(); // 2; read-write f
x.f() = 3;     // read-write f
cout << x.f(); // 3

const A cx(6);
cout << cx.f(); // 6; read-only f
cx.f() = 7;     // no





#ifdef TEST1
void test1 () {
    const Digits<int>           x = 0;
    const Digits<int>::iterator b = x.begin();
    const Digits<int>::iterator e = x.end();
    assert(b == e);}
#endif

#ifdef TEST2
void test2 () {
    Digits<int>           x = 2;
    Digits<int>::iterator b = x.begin();
    Digits<int>::iterator e = x.end();
    assert(b  != e);
    assert(*b == 2);
    Digits<int>::iterator& p = ++b;
    assert(&p == &b);
    assert(b  == e);}
#endif

#ifdef TEST3
void test3 () {
    Digits<int>           x = 23;
    Digits<int>::iterator b = x.begin();
    Digits<int>::iterator e = x.end();
    assert(b  != e);
    assert(*b == 3);
    ++b;
    assert(b  != e);
    assert(*b == 2);
    Digits<int>::iterator p = b++;
    assert(&p != &b);
    assert(b  == e);}
#endif

#ifdef TEST4
void test4 () {
    Digits<int>           x = 234;
    Digits<int>::iterator b = x.begin();
    Digits<int>::iterator e = x.end();
    assert(my_equal(b, e, begin({4, 3, 2})));}
#endif

#ifdef TEST5
void test5 () {
    Digits<int> x = 234;
    int s = 0;
    for (int v : x)
        s += v;
    assert(s == 9);}
#endif





/*
cat

never finished

last airbender
*/
