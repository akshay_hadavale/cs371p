// -----------
// Mon, 19 Feb
// -----------

/*
Reminders
    Sun, 25 Feb: Blog  #6
    Sun, 25 Feb: Paper #6: The Open-Closed Principle

    UT Programming Club (UTPC)
    Contest #3 (team)
    F, 1 Mar, 5:30 pm, GDC 2.216
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/

/*
5 types of iterator

input
    ==, !=, * (read only), ++
output
    * (write only), ++
forward
    ==, !=, * (read/write), ++
bidirectional
    ==, !=, * (read/write), ++, --
random access
    ==, !=, * (read/write), ++, --, [], +, -, <, <=, >, >=
*/

// -----------
// Reverse.cpp
// -----------

struct input_iterator_tag {};

struct output_iterator_tag {};

struct forward_iterator_tag : input_iterator_tag {};

struct bidirectional_iterator_tag : forward_iterator_tag {};

struct random_access_iterator_tag : bidirectional_iterator_tag {};





template <typename T>
struct forward_list {
    struct iterator {
        using iterator_category = forward_iterator_tag;
            ...};
    ...};

template <typename T>
struct list {
    struct iterator {
        using iterator_category = bidirectional_iterator_tag;
            ...};
    ...};

template <typename T>
struct vector {
    struct iterator {
        using iterator_category = random_access_iterator_tag;
            ...};
    ...};





template <typename BI>
void my_reverse_aux (BI b, BI e, bidirectional_iterator_tag) {
    while ((b != e) && (b != --e)) {
        swap(*b, *e);
        ++b;}}

template <typename RI>
void my_reverse_aux (RI b, RI e, random_access_iterator_tag) {
    while (b < --e) {
        swap(*b, *e);
        ++b;}}





template <typename I>
struct iterator_traits {
    using iterator_category = typename I::iterator_category;};

template <typename T>
struct iterator_traits<T*> {
    using iterator_category = random_access_iterator_tag;};





template <typename I>
void my_reverse (I b, I e) {
    my_reverse_aux(b, e, typename iterator_traits<I>::iterator_category());}





void test1 () {
    int a[] = {2, 3, 4};
    my_reverse(a, a);
    assert(equal(a, a + 3, begin({2, 3, 4})));}
    I -> int*

void test2 () {
    int a[] = {2, 3, 4};
    my_reverse(a, a + 1);
    assert(equal(a, a + 3, begin({2, 3, 4})));}

void test3 () {
    vector<int>           x = {2, 3, 4};
    vector<int>::iterator e = begin(x);
    ++++e;
    my_reverse(begin(x), e);
    assert(equal(begin(x), end(x), begin({3, 2, 4})));}
    I -> vector<int>::iterator

void test4 () {
    vector<int> x = {2, 3, 4};
    my_reverse(begin(x), end(x));
    assert(equal(begin(x), end(x), begin({4, 3, 2})));}

void test5 () {
    list<int>           x = {2, 3, 4};
    list<int>::iterator e = begin(x);
    ++++e;
    my_reverse(begin(x), e);
    assert(equal(begin(x), end(x), begin({3, 2, 4})));}
    I -> list<int>::iterator

void test6 () {
    list<int> x = {2, 3, 4};
    my_reverse(begin(x), end(x));
    assert(equal(begin(x), end(x), begin({4, 3, 2})));}





/*
meditations

flip turn

mr. & mrs. smith

dumb money

obituary
*/
