// -----------
// Wed,  6 Mar
// -----------

/*
Reminders
    Wed,  6 Mar: Project      #3: Allocator
    Sun, 10 Mar: Blog         #8
    Sun, 10 Mar: Paper        #8. The Integration Segregation Principle
    Fri, 22 Mar: UTPC Contest #4 (individual)
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/

/*
5 types of iterator

input
    ==, !=, * (read only), ++
output
    * (write only), ++
forward
    ==, !=, * (read/write), ++
bidirectional
    ==, !=, * (read/write), ++, --
random access
    ==, !=, * (read/write), ++, --, [], +, -, <, <=, >, >=
*/

/*
Last Class
    Exercise #5: array

This Class
    initializations
    arrays on the heap
*/

// -------------------
// Initializations.cpp
// -------------------

void test1 () {
    int i(2);
    assert(i == 2);

    int j = 2;
    assert(j == 2);

    int k{2};
    assert(k == 2);

    int l = {2};
    assert(l == 2);}



void test2 () {
    int i(2.0);
    assert(i == 2);

    int j = 2.0;
    assert(j == 2);

//  int k{2.0};     // error: narrowing conversion of '2.0e+0' from 'double' to 'int'

//  int l = {2.0};  // error: narrowing conversion of '2.0e+0' from 'double' to 'int'
    }



struct A {
    T a[5];
    int i;
    double d;

//  A             ()         = default; // default constructor
//  A             (const A&) = default; // copy constructor
//  A& operator = (const A&) = default; // copy assignment operator
//  ~A            ()         = default; // destructor
    };

void test3 () {
//  A x(2);      // error: no matching constructor for initialization of 'A'

//  A y = 2;     // error: no viable conversion from 'int' to 'A'

    A z{{...}, 2, 3.4};
    assert(&z);

    A t = {2, 3.4};
    assert(&t);}



struct B {
    static int c;

    int i;

    B             ()         = default; // default constructor
    B             (const B&) = default; // copy constructor
    B& operator = (const B&) = default; // copy assignment operator
    ~B            ()         = default; // destructor

    B (int) {
        ++c;}};

int B::c = 0;

void test4 () {
    B x(2);             // B(int)
    assert(B::c == 1);

    B y = 2;
    assert(&y);
    assert(B::c == 2);  // B(int)

    B z{2}   ;          // B(int)
    assert(B::c == 3);

    B t = {2};          // B(int)
    assert(B::c == 4);}



struct C {
    static int c;

    int i;

    C             ()         = default; // default constructor
    C             (const C&) = default; // copy constructor
    C& operator = (const C&) = default; // copy assignment operator
    ~C            ()         = default; // destructor

    explicit C (int) {
        ++c;}};

int C::c = 0;

void test5 () {
    C x(2);            // C(int)
    assert(C::c == 1);

//  C y = 2;           // error: no viable conversion from 'int' to 'C'

    C z{2};            // C(int)
    assert(C::c == 2);

//  C t = {2};         // error: chosen constructor is explicit in copy-initialization
    }



struct D {
    static int c;

    int i;

    D             ()         = default; // default constructor
    D             (const D&) = default; // copy constructor
    D& operator = (const D&) = default; // copy assignment operator
    ~D            ()         = default; // destructor

    D (initializer_list<int>) {
        ++c;}};

int D::c = 0;

void test6 () {
//  D x(2);             // error: no matching constructor for initialization of 'D'

//  D y = 2;            // error: no viable conversion from 'int' to 'D'

    D z{2};             // D(initializer_list<int>)
    assert(D::c == 1);

    D t = {2};          // D(initializer_list<int>)
    assert(D::c == 2);}



struct E {
    static int c;

    int i;

    E             ()         = default; // default constructor
    E             (const E&) = default; // copy constructor
    E& operator = (const E&) = default; // copy assignment operator
    ~E            ()         = default; // destructor

    explicit E (initializer_list<int>) {
        ++c;}};

int E::c = 0;

void test7 () {
//  E x(2);            // error: no matching constructor for initialization of 'E'

//  E y = 2;           // error: no viable conversion from 'int' to 'E'

    E z{2};            // E(initializer_list<int>)
    assert(E::c == 1);

//  E t = {2};         // error: chosen constructor is explicit in copy-initialization
    }



struct F {
    static int c;

    int i;

    F             ()         = default; // default constructor
    F             (const F&) = default; // copy constructor
    F& operator = (const F&) = default; // copy assignment operator
    ~F            ()         = default; // destructor

    F (int) {
        c += 2;}

    F (initializer_list<int>) {
        c += 3;}};

int F::c = 0;

void test8 () {
    F x(2);              // F(int)
    assert(F::c == 2);

    F y = 2;             // F(int)
    assert(&y);
    assert(F::c == 4);

    F z{2};              // F(initializer_list<int>)
    assert(F::c == 7);

    F t = {2};           // F(initializer_list<int>)
    assert(F::c == 10);}



void test9 () {
    vector<int> x(2);                               // vector<int>(int)
    assert(x.size() == 2);
    assert(equal(begin(x), end(x), begin({0, 0})));

//  vector<int> y = 2;                              // error: conversion from 'int' to non-scalar type 'std::vector<int>' requested

    vector<int> z{2};                               // vector<int>(initializer_list<int>)
    assert(z.size() == 1);
    assert(equal(begin(z), end(z), begin({2})));

    vector<int> t = {2};                            // vector<int>(initializer_list<int>)
    assert(t.size() == 1);
    assert(equal(begin(t), end(t), begin({2})));}





template <typename T, size_t N>
struct my_array {
    friend bool operator == (const my_array& lhs, const my_array& rhs) {
        return equal(lhs.begin(), lhs.end(), rhs.begin());}

    T _a[N];

    T& operator [] (size_t i) {
        return _a[i];}

    const T& operator [] (size_t i) const {
        return _a[i];}

    T* begin () {
        return _a;}

    const T* begin () const {
        return _a;}

    T* end () {
        return _a + N;}

    const T* end () const {
        return _a + N;}

    size_t size () const {
        return N;}};



// -----------
// Arrays2.cpp
// -----------

void f (int p[]) {
    assert(sizeof(p) == 8); // warning: sizeof on array function parameter will return size of 'int *' instead of 'int []'
    ++p;
    ++p[0];
    ++*p;}

void test1 () {
    const ptrdiff_t s = 10;
    const int       v =  2;
    int*  const     a = new int[s];  // O(1)
    T*    const     a = new T[s];    // O(n), T(), s times
    assert(sizeof(a) == 8);
    fill(a, a + s, v);               // O(n), =(T), s times
    assert(count(a, a + s, v) == s); // O(n), ==(T), s times
    assert(a[1] == v);
    f(a);
    assert(a[1] == v + 2);
    delete a;     // no
    delete [] a;} // O(1) or O(n), ~T(), s times

void g (int* p) {
    assert(sizeof(p) == 8);
    ++p;
    ++p[0];
    ++*p;}

void test2 () {
    const ptrdiff_t s = 10;
    const int       v = 2;
    int* const      a = new int[s];
    assert(sizeof(a) == 8);
    fill(a, a + s, v);
    assert(count(a, a + s, v) == s);
    assert(a[1] == v);
    g(a);
    assert(a[1] == v + 2);
    delete [] a;}

void test3 () {
    const size_t  s = 10;
    const int     v = 2;
    int*  const   a = new int[s];
    assert(sizeof(a) == 8);
    fill(a, a + s, v);
    int* const b = a;
    assert(&a[1] == &b[1]);
    delete [] a;}

void test4 () {
    const size_t  s = 10;
    const int     v = 2;
    int*  const   a = new int[s];
    assert(sizeof(a) == 8);
    fill(a, a + s, v);
    int* const x = new int[s];
    copy(a, a + s, x);
    assert( a[1] ==  x[1]);
    assert(&a[1] != &x[1]);
    delete [] a;
    delete [] x;}

void test5 () {
    const size_t s = 10;
    const int    v =  2;
    int*  const  a = new int[s];
    assert(sizeof(a) == 8);
    fill(a, a + s, v);
    int* b = new int[s];
    fill(b, b + s, v);
//  b = a;                       // memory leak
    copy(a, a + s, b);
    assert( a[1] ==  b[1]);
    assert(&a[1] != &b[1]);
    delete [] a;
    delete [] b;}

struct A {
    int i;

    string f () {
        return "A::f";}};

struct B : A {
    int j;

    string f () {
        return "B::f";}};

void test6 () {
//  B* const a = new A[10];     // error: invalid conversion from ‘A*’ to ‘B*’
    A* const a = new B[10];     // dangerous
    assert(a[0].f() == "A::f");
//  assert(a[1].f() == "A::f"); // undefined
//  delete [] a;                // undefined
    B* b = static_cast<B*>(a);
    assert(b[1].f() == "B::f");
    delete [] b;}               // ~B::B() and ~A::A()





/*
wetzel's pretzels

up

factorio

turkey leg hut

one day

the left hand of darkness

risk of rain 2

queen's gambit
*/
