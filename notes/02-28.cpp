// -----------
// Wed, 28 Feb
// -----------

/*
Reminders
    Fri,  1 Mar: UTPC Contest #3 (team)
    Sun,  3 Mar: Blog         #7
    Sun,  3 Mar: Paper        #7: The Liskov Substitution Principle
    Wed,  6 Mar: Project      #3: Allocator
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/

/*
5 types of iterator

input
    ==, !=, * (read only), ++
output
    * (write only), ++
forward
    ==, !=, * (read/write), ++
bidirectional
    ==, !=, * (read/write), ++, --
random access
    ==, !=, * (read/write), ++, --, [], +, -, <, <=, >, >=
*/

/*
Last Class
    Exercise #4: Digits

This Class
    operator ==
*/

struct A {
    };

A x;
A y;
cout << (x == y);        // no
cout << x.operator==(y); // no





struct A {
    bool operator == (const A& rhs) const {
        ...}

const A x;
const A y;
cout << (x == y);
cout << x.operator==(y);





class string {
    private:
        ...
    public:
        string (const char* a) { // constructor and type converter
            ...}};

string s = "abc";

void f (string t) {
    ...}

f(s);
f("abc");





template <typename T>
class vector {
    private:
        ...
    public:
        explicit vector (size_t s) {
            ...}
        ...};

vector<int> x(10000);

void f (vector<int> w) {
    ...}

f(x);
f(10000); // no





class string {
    private:
        ...
    public:
        bool operator == (const string& rhs) const {
            ...}

        string (const char* a) { // constructor and type converter
            ...}};

string s = "abc";
strint t = "abc";
cout << (s == t);        // true
cout << s.operator==(t); // true

cout << (s == "abc");        // true
cout << s.operator==("abc"); // true

cout << ("abc" == s); // no
cout << "abc".





class string {
    private:
        ...
    public:
        friend bool operator == (const string& lhs, const string& rhs) {
            ...}

        string (const char* a) { // constructor and type converter
            ...}};

string s = "abc";
strint t = "abc";
cout << (s == t);         // true
cout << operator==(s, t); // true

cout << (s == "abc");         // true
cout << operator==(s, "abc"); // true

cout << ("abc" == s);      // true
cout << operator("abc", s) // true





/*
elysium

friren beyond

the lobster

pretty little liars

the curse
*/
