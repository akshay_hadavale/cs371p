// -----------
// Mon,  4 Mar
// -----------

/*
Reminders
    Wed,  6 Mar: Project      #3: Allocator
    Sun, 10 Mar: Blog         #8
    Sun, 10 Mar: Paper        #8. The Integration Segregation Principle
    Fri, 22 Mar: UTPC Contest #4 (individual)
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/

/*
5 types of iterator

input
    ==, !=, * (read only), ++
output
    * (write only), ++
forward
    ==, !=, * (read/write), ++
bidirectional
    ==, !=, * (read/write), ++, --
random access
    ==, !=, * (read/write), ++, --, [], +, -, <, <=, >, >=
*/

/*
Last Class
    arrays
    std::array

This Class
    Exercise #5: array
*/

template <typename T, size_t N>
class my_array {
    friend bool operator == (const my_array& lhs, const my_array& rhs) {
        return equal(lhs.begin(), lhs.end(), rhs.begin());}

    private:
        T _a[N];

    public:
        my_array             ()                = default; // default constructor
        my_array             (const my_array&) = default; // copy    constructor
        my_array& operator = (const my_array&) = default; // copy    assignment
        ~my_array            ()                = default; // destructor

        my_array (initializer_list<T> rhs) {
            copy(rhs.begin(), rhs.end(), begin());}

        T& operator [] (size_t i) {
            return _a[i];}

        const T& operator [] (size_t i) const {
            return _a[i];}

        T* begin () {
            return _a;}

        const T* begin () const {
            return _a;}

        T* end () {
            return _a + N;}

        const T* end () const {
            return _a + N;}

        size_t size () const {
            return N;}};

#ifdef TEST1
void test1 () {
    my_array<int, 3> x;
    assert(x.size() == 3);}
#endif

#ifdef TEST2
void test2 () {
    my_array<int, 3> x = {2, 3, 4};
    assert(x[1] == 3);
    x[1] = 5;
    assert(x[1] == 5);}
#endif

#ifdef TEST3
void test3 () {
    const my_array<int, 3> x = {2, 3, 4};
    assert(x[1] == 3);
//  x[1] = 5;                             // error: assignment of read-only location 'x.my_array<int, 3>::operator[](1)'
    assert(x[1] == 3);}
#endif

#ifdef TEST4
void test4 () {
    const my_array<int, 3> x = {2, 3, 4};
    assert(equal(begin(x), end(x), begin({2, 3, 4})));}
#endif

#ifdef TEST5
void test5 () {
    my_array<int, 3>       x = {2, 3, 4};
    const my_array<int, 3> y = x;
    assert(&*begin(x) != &*begin(y));
    assert(equal(begin(y), end(y), begin({2, 3, 4})));}
#endif

#ifdef TEST6
void test6 () {
    const my_array<int, 3> x = {2, 3, 4};
    my_array<int, 3>       y = {5, 6, 7};
    y = x;
    assert(&*begin(x) != &*begin(y));
    assert(equal(begin(y), end(y), begin({2, 3, 4})));}
#endif

#ifdef TEST7
void test7 () {
    const my_array<int, 5> x = {2, 3, 4, 5, 6};
    const my_array<int, 5> y = {2, 3, 4, 5, 6};
    assert(  x == y);
    assert(!(x != y));}
#endif

#ifdef TEST8
void test8 () {
    const my_array<int, 5> x = {2, 3, 4, 5, 6};
    const my_array<int, 5> y = {2, 3, 4, 5, 7};
    assert(  x != y);
    assert(!(x == y));}
#endif





/*
The Office

one piece
*/
