// -----------
// Mon,  5 Feb
// -----------

/*
Reminders
    Sun, 11 Feb: Blog  #4
    Sun, 11 Feb: Paper #4

    UT Programming Club (UTPC)
    Contest #2
    F, 9 Feb, 5:30 pm, GDC 2.216
*/

// --------------
// Exceptions.cpp
// --------------

// https://en.cppreference.com/w/cpp/language/exceptions

int f (bool b) {
    if (b)
        throw domain_error("abc"); // creating an instance
    return 0;}                     // invoking a constructor
                                   // always makes a copy

void test1 () {
    try {
        assert(f(false) == 0);
        }
    catch (domain_error& e) {
        assert(false);}}

void test2 () {
    try {
        assert(f(true) == 0);
        assert(false);
        }
    catch (domain_error& e) {
//      assert(       e                == "abc");   // error: no match for ‘operator==’ in ‘e == "abc"’
//      assert(       e.what()         != "abc");   // warning: comparison with string literal results in unspecified behavior
        assert(strcmp(e.what(), "abc") == 0);       // int's ==
        assert(string(e.what())        == "abc");}}

void test3 () {
    domain_error x("abc");
    x.foo();
    logic_error& y = x;    // logic_error is a parent of domain_error
    exception&   z = y;
    assert(&x == &z);}





/*
there is NO == for domain_error and char*
what is the return type of domain_error::what()? char*
*/

int strcmp (const char*, const char*) {
    // loop
    ...}

char*  a = "abc"; // c-string
string s = "abc"; // c++-string
string t = "abc";
cout << (s == t); // c++-string's == that takes two c++-strings
cout << (s == a); // promotes a c-string to c++-string





int    i = 2;
double d = 3.4;
cout << (i == d); // promotes an int to a double





void f (string t) {
    ...}

string s = "abc";
f(s);
f("abc");         // promotes a c-string to a c++-string





/*
two tokens:
    *, &
two contexts
    modifying a variable
    modifying a type
*/

int i; // initialization is NOT required
i = 2;

int* p; // initialization is NOT required; * on a type declares a pointer of that type
p = i;  // no
p = &i; // & on a variable creates an address, variable must be an l-value

cout << i;  // 2
cout << *i; // no

cout << p;  // prints the pointer
cout << *p; // 2, * on a variable dereferences, variable must be a pointer

int j = 3;
p = j;     // no
p = &j;    // pointers can point to many targets

int& r;      // no, declaring a reference, initialization IS required
int& r = &i; // no
int& r = i;

cout << (&i == &r); // yes!!!!!





/*
I School, pokeman

College Station, volleyball

Memorial, basketball

Village, cooking
*/
