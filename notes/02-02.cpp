// -----------
// Fri,  2 Feb
// -----------

/*
Reminders
    Sun, 4 Feb: Blog  #3
    Sun, 4 Feb: Paper #3: Continuous Integration

    UT Programming Club (UTPC)
    Contest #1
    F, 9 Feb, 5:30 pm, GDC 2.216
*/

/*
1. return
2. globals
3. parameters

the caller is NOT required to notice
*/

/*
exceptions

the caller is REQUIRED to notice
*/

/*
user-defined types
    in Java only on the heap
    in C++ on the stack or the heap
*/





int f (...) {
    ...
    if (<something wrong>)
        throw E1();        // creating an instance of class E1 on the stack
    ...

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (E1) {
        <something wrong}
    ...

/*
f does NOT throw
rest of f: yes
rest of try: yes
catch: no
rest of g: yes
*/

/*
f throws E1
rest of f: no
rest of try: no
catch: yes
rest of g: yes
*/





int f (...) {
    ...
    if (<something wrong>)
        throw E2();        // creating an instance of class E2 on the stack
    ...

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (E1) {
        <something wrong}
    ...

/*
f throws E2
rest of f: no
rest of try: no
catch: no
rest of g: no
go to the caller of g
*/





int f (...) {
    ...
    if (<something wrong>)
        throw Tiger();     // creating an instance of class Tiger on the stack
    ...

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Mammal) {
        <something wrong}
    catch (Tiger) {       // this will never happen
        <something wrong}
    ...

/*
f throws Tiger
rest of f: no
rest of try: no
catch: Mammal!!!
rest of g: yes
*/





// Always list child first

int f (...) {
    ...
    if (<something wrong>)
        throw Tiger();     // creating an instance of class Tiger on the stack
    ...

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Tiger) {
        <something wrong}
    catch (Mammal) {
        <something wrong}
    ...

/*
f does throw a Tiger
rest of f: no
rest of try: no
catch: Tiger
rest of g: yes
*/





// do NOT catch by VALUE

int f (...) {
    ...
    if (<something wrong>)
        throw Tiger();     // creating an instance of class Tiger on the stack
                           // Tiger will die
                           // throw ALWAYS makes a copy
    ...}

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Mammal) {       // catch by value, make a copy, two copies!!!
        <something wrong}  // there's only enough room for a Mammal object
                           // on this catcher's stack
                           // lose the Tiger data!!!
    ...

/*
f throws Tiger
rest of f: no
rest of try: no
catch: Mammal, ONLY Mammal data
rest of g: yes
*/





// do NOT catch by ADDRESS

int f (...) {
    ...
    if (<something wrong>)
        // throw Tiger();     // creating an instance of class Tiger on the stack
        // Tiger x(...);
        // throw &x;          // x will die
        throw new Tiger(...); // caller is now responsible for calling delete!!!
    ...}

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Mammal*) {         // catch by address, now I'm responsible for deleting
        <something wrong}
    ...

/*
f throws Tiger
rest of f: no
rest of try: no
catch: no
rest of g: no
go to the caller of g
*/





// ALWAYS catch by REFERENCE

int f (...) {
    ...
    if (<something wrong>)
        throw Tiger();     // creating an instance of class Tiger on the stack
    ...}

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Mammal&) {      // catch by reference
        <something wrong}  // Mammal alias of Tiger, NOT data lost
    ...

/*
f throws Tiger
rest of f: no
rest of try: no
catch: yes
rest of g: yes
*/





/*
Bellaire, running

North Garland, pickle ball

Oregon, basketball

Dripping Springs, Bottoms

Clements, sports

Harmony, Lethal Company
*/
