// -----------
// Mon, 29 Jan
// -----------

/*
Reminders
    Sun, 4 Feb: Blog  #3
    Sun, 4 Feb: Paper #3

    UT Programming Club (UTPC)
    Contest #1
    F, 9 Feb, 5:30 pm, GDC 2.216
*/

++i++; // no, postfix has a higher precedence than prefix





/*
broken tests can hide broken code
*/

bool is_prime (int n) {
    assert(n > 0);
    if (n == 2)
        return true;
    if ((n == 1) || ((n % 2) == 0))
        return false;
    for (int i = 3; i < std::sqrt(n) + 1; ++++i)
        if ((n % i) == 0)
            return false;
    return true;}

void test1 () {
    assert(!is_prime( 1));}

void test2 () {
    assert(is_prime( 2));}

void test3 () {
    assert( is_prime( 3));}

void test4 () {
    assert(!is_prime( 4));}

void test5 () {
    assert( is_prime( 5));}

void test7 () {
    assert( is_prime( 7));}

void test9 () {
    assert(!is_prime( 9));}

void test27 () {
    assert(!is_prime(27));}

void test29 () {
    assert( is_prime(29));}

/*
1. run it as is, confirm success
2. identify and fix the tests, remember
3. run it again, confirm failure
4. identify and fix the code, remember
5. run it again, confirm success
*/

/*
submit as many times as you like
20 min
before submitting ask Yundi or me to take a look
*/





// pass by value

void f (int v) {
    ++v;}

voig g () {
    int i = 2;
    g(i);
    cout << i;} // 2





// pass by address

void f (int* p) {
    assert(p);
    ++*p;}

voig g () {
    int i = 2;
    g(&i);
    g(0);       // runtime error
    cout << i;} // 3





// pass by reference

void f (int& r) { // must be an l-value
    ++*r;         // compile-time error
    ++r;}

voig g () {
    int i = 2;
    g(i);
    g(0);       // compile-time error
    cout << i;} // 3





/*
McNeil, walking

Vandergriff, basketball

Dobie, bouldering
*/
