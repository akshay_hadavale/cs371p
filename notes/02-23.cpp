// -----------
// Fri, 23 Feb
// -----------

/*
Reminders
    Sun, 25 Feb: Blog         #6
    Sun, 25 Feb: Paper        #6: The Open-Closed Principle
    Fri,  1 Mar: UTPC Contest #3 (team)
    Wed,  6 Mar: Project      #3: Allocator
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/

/*
5 types of iterator

input
    ==, !=, * (read only), ++
output
    * (write only), ++
forward
    ==, !=, * (read/write), ++
bidirectional
    ==, !=, * (read/write), ++, --
random access
    ==, !=, * (read/write), ++, --, [], +, -, <, <=, >, >=
*/

/*
Last Class
    Allocator project

This Class
    Exercise #4: Digits
*/

#ifdef TEST1
void test1 () {
    const Digits<int>           x = 0;
    const Digits<int>::iterator b = x.begin();
    const Digits<int>::iterator e = x.end();
    assert(b == e);}
#endif

#ifdef TEST2
void test2 () {
    Digits<int>           x = 2;
    Digits<int>::iterator b = x.begin();
    Digits<int>::iterator e = x.end();
    assert(b  != e);
    assert(*b == 2);
    Digits<int>::iterator& p = ++b;
    assert(&p == &b);
    assert(b  == e);}
#endif

#ifdef TEST3
void test3 () {
    Digits<int>           x = 23;
    Digits<int>::iterator b = x.begin();
    Digits<int>::iterator e = x.end();
    assert(b  != e);
    assert(*b == 3);
    ++b;
    assert(b  != e);
    assert(*b == 2);
    Digits<int>::iterator p = b++;
    assert(&p != &b);
    assert(b  == e);}
#endif

#ifdef TEST4
void test4 () {
    Digits<int>           x = 234;
    Digits<int>::iterator b = x.begin();
    Digits<int>::iterator e = x.end();
    assert(my_equal(b, e, begin({4, 3, 2})));}
#endif

#ifdef TEST5
void test5 () {
    Digits<int> x(234);
    int s = 0;
    for (int v : x)
        s += v;
    assert(s == 9);}
#endif

template <typename T>
class Digits {
	private:
		T _v;

	public:
		class iterator {
			iterator () {
				T _v;}

			operator !=
 			operator *
			operator ++ ()
			operator ++ (int) // postfix

		Digits (const T& v) :
			_v = v;}          // T(), =(T)

		Digits (const T& v) :
			_v (v) {}         // T(T)

		begin () const
		end () const





/*
the big short

the seven husbands of evelyn hudo

psych
*/
