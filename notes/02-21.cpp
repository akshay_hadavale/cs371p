// -----------
// Wed, 21 Feb
// -----------

/*
Reminders
    Sun, 25 Feb: Blog  #6
    Sun, 25 Feb: Paper #6: The Open-Closed Principle

    UT Programming Club (UTPC)
    Contest #3 (team)
    F, 1 Mar, 5:30 pm, GDC 2.216
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/

/*
5 types of iterator

input
    ==, !=, * (read only), ++
output
    * (write only), ++
forward
    ==, !=, * (read/write), ++
bidirectional
    ==, !=, * (read/write), ++, --
random access
    ==, !=, * (read/write), ++, --, [], +, -, <, <=, >, >=
*/

int* a = new int[s]; // heap, <undefined>, O(1)

struct A {
    A() {...}
    ~A() {...}};

T* a = new T[s]; // heap, T(), default constructor, s times, O(n)
fill(a, a+s, v); // =(T), copy assignment operator, s times, O(n)
...
delete [] a;     // ~T(), destructor, s times, O(N)

template <typename I, typename T>
void fill (I b, I e, const T& v) {
    while (b != e) {
        *b = v;
        ++b;}}





vector<T> x(s, v); // O(n), NOT 2 x O(n)





allocator<T> x;
/*
allocate
construct
destroy
deallocate
*/

T* a = x.allocate(s);        // O(1), NO operation of T
for (int i = 0; i != s; ++i) // O(n)
    x.construct(a+i, v);     // O(1), T(T), copy constructor
...
for (int i = 0; i != s; ++i) // O(n)
    x.destroy(a+i);          // O(1), ~T(), destructor
x.deallocate(a);             // O(1), NO operation of T





template <typename T, size_T N>
class Allocator {
    private:
        char a[N]; // array of N bytes
    ...};





// array of 100 bytes

allocator<double, 100> x;
[92...92]

// allocates 5 doubles

double* p = x.allocate(5); // 40 bytes, O(n), NO operation of T
                           // linear search to find the first free block that is big enough
                           // if there isn't enough room left over for a free block of at least 1 T
                           // give the entire free block

    p
    |
48  v      52
[-40...-40][44...44]

double* p = x.allocate(5); // 40 bytes





/*
i think you should leave

the strokes

the goldfinch

the meaning of marriage

peaky blinders

lumineries

the bell jar
*/
