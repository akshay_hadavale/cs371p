// -----------
// Wed, 20 Mar
// -----------

/*
Reminders
    Fri, 22 Mar: UTPC Contest #4 (individual)
    Sun, 24 Mar: Blog         #9
    Sun, 24 Mar: Paper        #9. The Dependency Inversion Principle
    Wed,  3 Apr: Project      #4:
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/

/*
5 types of iterator

input
    ==, !=, * (read only), ++
output
    * (write only), ++
forward
    ==, !=, * (read/write), ++
bidirectional
    ==, !=, * (read/write), ++, --
random access
    ==, !=, * (read/write), ++, --, [], +, -, <, <=, >, >=
*/

/*
Last Class
    Exercise #6: Vector

This Class
*/

