// -----------
// Fri, 19 Jan
// -----------

/*
Reminders
    Sun, 21 Jan: Blog  #1
    Sun, 21 Jan: Paper #1: Syllabus

    UT Programming Club (UTPC)
    Beginners' Workshop
    Th, 25 Jan, 6 pm, GDC auditorium

    UT Programming Club (UTPC)
    Contest #1
    F, 26 Jan, 5:30 pm, GDC auditorium
*/

/*
how to indicate amount of input
    tell me up front
    EOF, !cin
    tell me at the end
*/

/*
read eval print loop (REPL)
*/

/*
skeleton code

run harness, run_Grades.cpp: grades_read(), grades_print()
    handles input and output

test harness, test_Grades.cpp, a set of unit tests
    set of unit tests, which test individual functions

kernel, Grades.hpp: grades_eval()
*/

/*
acceptance test

% run_Grades < input.txt > output.txt
% diff output.txt expected_output.text
*/

/*
I need several more unit tests in test_Grades.py

gpdowning-Grades.in.txt
gpdowning-Grades.out.txt

<your gitlab id>-Grades.in.txt, many, many test cases
<your gitlab id>-Grades.out.txt
*/

/*
add more issues
    bugs
    enhancements
*/

/*
when you have a bug
    create an issue
    write a test that fails because of the bug
    fix the bug
    mark the issue resolved
*/

/*
Timbercreek, sports
College park, dodge ball
Thomas Jefferson, spikeball
Aikens, league of legends
Plano Senior, basketball
Bellaire, edit videos
McNeil, art
*/
