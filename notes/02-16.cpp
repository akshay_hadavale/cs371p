// -----------
// Fri, 16 Feb
// -----------

/*
Reminders
    Sun, 18 Feb: Blog  #5
    Sun, 18 Feb: Paper #5: The Single Responsibility Principle

    UT Programming Club (UTPC)
    Contest #3 (team)
    F, 1 Mar, 5:30 pm, GDC 2.216
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/

/*
5 types of iterator

input
    ==, !=, * (read only), ++
output
    * (write only), ++
forward
    ==, !=, * (read/write), ++
bidirectional
    ==, !=, * (read/write), ++, --
random access
    ==, !=, * (read/write), ++, --, [], +, -, <, <=, >, >=
*/





/*
iterators are the glue between algorithms and containers
*/

// algorithms

bool equal (II1 b1, II1 e1, II2 b2) // input

void fill (FI b, FI e, T v) // forward

void reverse (BI b, BI e) // bidirectional

void sort (RI b, RI e) // random access

// containers

class forward_list // forward

class list // bidirectional

class vector // random access





template <typename RI>
void my_reverse (RI b, RI e) {
    while (b < --e) {
        swap(*b, *e);
        ++b;}}

#ifdef TEST1
void test1 () {
    int a[] = {2, 3, 4};
    my_reverse(a, a);
    assert(equal(a, a + 3, begin({2, 3, 4})));}
#endif

#ifdef TEST2
void test2 () {
    int a[] = {2, 3, 4};
    my_reverse(a, a + 1);
    assert(equal(a, a + 3, begin({2, 3, 4})));}
#endif

#ifdef TEST3
void test3 () {
    vector<int>           x = {2, 3, 4};
    vector<int>::iterator b = begin(x);
    vector<int>::iterator e = begin(x);
    e += 2;
    my_reverse(b, e);
    assert(equal(begin(x), end(x), begin({3, 2, 4})));}
#endif

#ifdef TEST4
void test4 () {
    vector<int> x = {2, 3, 4};
    my_reverse(begin(x), end(x));
    assert(equal(begin(x), end(x), begin({4, 3, 2})));}
#endif

#ifdef TEST5
void test5 () {
    vector<int> x = {2, 3, 4, 5};
    my_reverse(begin(x), end(x));
    assert(equal(begin(x), end(x), begin({5, 4, 3, 2})));}
#endif





template <typename BI>
void my_reverse (BI b, BI e) {
    while ((b != e) && (b != --e)) {
        swap(*b, *e);
        ++b;}}

#ifdef TEST1
void test1 () {
    int a[] = {2, 3, 4};
    my_reverse(a, a);
    assert(equal(a, a + 3, begin({2, 3, 4})));}
#endif

#ifdef TEST2
void test2 () {
    int a[] = {2, 3, 4};
    my_reverse(a, a + 1);
    assert(equal(a, a + 3, begin({2, 3, 4})));}
#endif

#ifdef TEST3
void test3 () {
    list<int>           x = {2, 3, 4};
    list<int>::iterator b = begin(x);
    list<int>::iterator e = begin(x);
    ++++e;
    my_reverse(b, e);
    assert(equal(begin(x), end(x), begin({3, 2, 4})));}
#endif

#ifdef TEST4
void test4 () {
    list<int> x = {2, 3, 4};
    my_reverse(begin(x), end(x));
    assert(equal(begin(x), end(x), begin({4, 3, 2})));}
#endif

#ifdef TEST5
void test5 () {
    list<int> x = {2, 3, 4, 5};
    my_reverse(begin(x), end(x));
    assert(equal(begin(x), end(x), begin({5, 4, 3, 2})));}
#endif





/*
community

beef

the bachelor
*/
