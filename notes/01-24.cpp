// -----------
// Wed, 24 Jan
// -----------

/*
Reminders
    Sun, 28 Jan: Blog  #2
    Sun, 28 Jan: Paper #2: Makefile

    UT Programming Club (UTPC)
    Beginners' Workshop
    Th, 25 Jan, 6 pm, GDC auditorium

    UT Programming Club (UTPC)
    Contest #1
    F, 26 Jan, 5:30 pm, GDC auditorium
*/

/*
assertions
unit tests
r-values / l-values
*/



/*
Collatz Conjecture
*/

/*
take a pos int
if even, divide it by 2
if odd,  multiply it by 3 and add 1
repeat until 1
*/

1
5 16 8 4 2 1
10 5 16 8 4 2 1

/*
the cycle length of  1 is 1
the cycle length of  5 is 6
the cycle length of 10 is 7
*/



// --------------
// Assertions.cpp
// --------------

// https://en.cppreference.com/w/cpp/error/assert

#include <cassert>  // assert
#include <iostream> // cout, endl

using namespace std;

int cycle_length (int n) {
    assert(n > 0); // check pre-condition
    int c = 0;
    while (n > 1) {
        if ((n % 2) == 0)
            n /= 2;
        else
            ++(n *= 3);
        ++c;}
    assert(c > 0); // check post-condition
    return c;}

void test () {
    assert(cycle_length( 1) == 1); // testing
    assert(cycle_length( 5) == 6);
    assert(cycle_length(10) == 7);}

int main () {
    cout << "Assertions.cpp" << endl;
    test();
    cout << "Done." << endl;
    return 0;}

/*
% clang++ --coverage -g -std=c++20 -I/usr/local/include -Wall -Wextra -Wpedantic Assertions.cpp -o Assertions -lgtest -lgtest_main
% ./Assertions
Assertions.cpp
Assertion failed: (c > 0), function cycle_length, file Assertions.cpp, line 21.



Turn OFF assertions at compile time with -DNDEBUG
% clang++ --coverage -g -std=c++20 -DNDEBUG -I/usr/local/include -Wall -Wextra -Wpedantic Assertions.cpp -o Assertions -lgtest -lgtest_main
% ./Assertions
Assertions.cpp
Done.
*/



/*
assertions are good for
    pre-conditions
    post-conditions
    loop variants

are not good for
    testing    (unit test framework, like Google Test)
    user input (what will be good exceptions)
*/



int i = 2;
int j = 3;

int k = (i + j);
int k = (2 + 3);

i = 2; // i is an l-value
       // 2 is an r-value

/*
an l-value is stronger than an r-value
*/

/*
+ takes two r-values
returns an r-value
*/

/*
<< takes two r-values
returns an r-value
*/

i + j; // no

k = (i + j);
(i + j) = k; // no

i += j;
i += 3;
2 += 3; // no

/*
+= takes an l-value and an r-value
returns an l-value
*/

/*
<<= takes an l-value and an r-value
returns an l-value
*/

i += j;
k = (i += j); // yes in C, C++, Java; NO in Python
(i += j) = k; // yes in C++; NO in C, Java, Python

i << j; // no
k = (i << j);

i <<= j;
(i <<= j) = k;



// --------------
// UnitTests1.cpp
// --------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 0;
    while (n > 1) {
        if ((n % 2) == 0)
            n /= 2;
        else
            ++(n *= 3);
        ++c;}
    assert(c > 0);
    return c;}

TEST(UnitTestsFixture, test0) {
    ASSERT_EQ(cycle_length( 1), 1);}

TEST(UnitTestsFixture, test1) {
    ASSERT_EQ(cycle_length( 5), 6);}

TEST(UnitTestsFixture, test2) {
    ASSERT_EQ(cycle_length(10), 7);}

/*
% clang++ --coverage -g -std=c++20 -I/usr/local/include -Wall -Wextra -Wpedantic UnitTests1.cpp -o UnitTests1 -lgtest -lgtest_main



% ./UnitTests1
Running main() from /tmp/googletest-20210612-68642-1k2ka09/googletest-release-1.12.1/googletest/src/gtest_main.cc
[==========] Running 3 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 3 tests from UnitTestsFixture
[ RUN      ] UnitTestsFixture.test0
Assertion failed: (c > 0), function cycle_length, file UnitTests1.cpp, line 22.
*/



/*
McNeil, baking

Morton Ranch, video game, baldurs gate 3

Montgomery, music, hall and oates

Advancement, spike ball

Westlake, basketball

Bellaire, movies, "The Bear"
*/
