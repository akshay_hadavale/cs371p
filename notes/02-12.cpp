// -----------
// Mon, 12 Feb
// -----------

/*
Reminders
    Sun, 19 Feb: Blog  #5
    Sun, 18 Feb: Paper #5

    UT Programming Club (UTPC)
    Contest #3 (team)
    F, 1 Mar, 5:30 pm, GDC 2.216
*/

// ----------
// Consts.cpp
// ----------

// https://en.cppreference.com/w/cpp/language/cv

#include <cassert>  // assert
#include <iostream> // cout, endl

using namespace std;

void test1 () {
    int i = 2;
    ++i;
    assert(i == 3);}

void test2 () {
//  const int ci;     // error: uninitialized const 'ci'
    const int ci = 4;
//  ++ci;             // error: increment of read-only variable 'ci'
    assert(ci == 4);}

void test3 () {
    // read/write, many-location pointer
    // mutable int, mutable pointer
    int       i  = 2;
    const int ci = 3;
    int*      p;
    p = &i;
    ++*p;
    assert(i == 3);
//  p = &ci;        // error: invalid conversion from 'const int*' to 'int*'
    assert(ci);}

void test4 () {
    // read-only, many-location pointer
    // immutable int, mutable pointer
    int        i  = 2;
    const int  ci = 3;
    const int* pc;
    pc = &ci;
//  ++*pc;                         // error: increment of read-only location
//  int* p = pc;                   // error: invalid conversion from 'const int*' to 'int*'
    int* p = const_cast<int*>(pc);
    assert(p == pc);
//  ++*p;                          // undefined
    pc = &i;                       // ?
    p = const_cast<int*>(pc);
    ++*p;
    assert(i == 3);}

void test5 () {
    // read/write, one-location pointer
    // mutable int, immutable pointer
    int        i  = 2;
    const int  ci = 3;
//  int* const cp;       // error: uninitialized const 'cp'
//  int* const cp = &ci; // error: invalid conversion from 'const int*' to 'int*'
    int* const cp = &i;
//  ++cp;                // error: cannot assign to variable 'cp' with const-qualified type 'int *const'
    ++*cp;
    assert(i == 3);
    assert(ci);}

void test6 () {
    // read-only, one-location pointer
    // immutable int, immutable pointer
    int              i   = 2;
    const int        ci  = 3;
//  const int* const cpc;       // error: uninitialized const 'cpc'
    const int* const cpc = &ci;
    const int* const cqc = &i;
//  ++cqc;                      // error: cannot assign to variable 'cqc' with const-qualified type 'const int *const'
//  ++*cqc;                     // error: increment of read-only location
    assert(cpc);
    assert(cqc);}

void test7 () {
    // read/write reference
    // mutable int
    int       i  = 2;
    const int ci = 3;
//  int&      r;      // error: 'r' declared as reference but not initialized
//  int&      r = ci; // error: invalid initialization of reference of type 'int&' from expression of type 'const int'
    int&      r = i;
    ++r;
    assert(i == 3);
    assert(ci);}

void test8 () {
    // read-only reference
    // immutable int
    int        i  = 2;
    const int  ci = 3;
//  const int& rc;      // error: 'rc' declared as reference but not initialized
    const int& rc = ci;
    const int& sc = i;
//  ++sc;               // error: increment of read-only reference 'sc'
    assert(rc);
    assert(sc);}





// -----------
// Arrays1.cpp
// -----------

// https://en.cppreference.com/w/cpp/container/array

#include <cstdio>

#include <algorithm> // copy, equal
#include <array>     // array
#include <cassert>   // assert
#include <iostream>  // cout, endl

using namespace std;

void test1 () {
    int a[] = {2, 3, 4}; // T* const

    T1 a[] = {2, 3, 4}; // T1(int), 3 times

    T2 a[] = {a, a, a}; // T2(int*), 3 times

    assert(*a == a[0]);
    assert(a  == &a[0]);
    assert(sizeof(a)     != sizeof(&a[0]));
    assert(sizeof(a)     == 12);
    assert(sizeof(&a[0]) == 8);
//  ++a;                                    // error: lvalue required as left operand of assignment
    ++a[1];
    ++*(a + 1);
    assert(a[1]     == 5);
    assert(*(a + 1) == 5);
//  assert(a[3]     == 0);                  // undefined
//  assert(*(a + 3) == 0);                  // undefined
    }





// Java

class A {}

A x = new A();

class B {
    B (int) {...}}

B x = new B(2);
B y = new B();  // no





void test2 () {
    const size_t s    = 10;
    const int    a[s] = {2, 3, 4};

    const T [s] = {2, 3, 4}; // T(int), 3 times; T(), 7 times

    assert(sizeof(a) == 40);
    assert(a[1]      ==  3);
    assert(*(a + 1)  ==  3);
    assert(a[s - 1]  ==  0);
//  ++a;                           // error: lvalue required as left operand of assignment
//  ++a[1];                        // error: increment of read-only location
    }

void test3 () {
    const size_t s = 10;
//  const int    a[s];       // error: uninitialized const 'a'
    int a[s];                // O(1), undefined ints

    T a[s]; // O(n), T(), 10 times

    assert(sizeof(a) == 40);
//  assert(a[0]      ==  0); // undefined
    }





/*
Westlake, swim

Seattle, jiu-jitsu

the anthropocene reviewed by John Green

astronaut's guide to life on earth

arcane

better call saul

before the coffee gets cold

big bang theory

breaking bad
*/
