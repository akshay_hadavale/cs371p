// -----------
// Reverse.cpp
// -----------

// http://en.cppreference.com/w/cpp/algorithm/reverse

#include <algorithm>  // equal, swap
#include <iterator>   // bidirectional_iterator_tag, iterator_traits, random_access_iterator_tag
#include <list>       // list
#include <vector>     // vector

#include "gtest/gtest.h"

using namespace std;

/*
struct input_iterator_tag {};

struct output_iterator_tag {};

struct forward_iterator_tag : input_iterator_tag {};

struct bidirectional_iterator_tag : forward_iterator_tag {};

struct random_access_iterator_tag : bidirectional_iterator_tag {};

template <typename T>
struct forward_list {
    struct iterator {
        using iterator_category = forward_iterator_tag;
            ...};
    ...};

template <typename T>
struct list {
    struct iterator {
        using iterator_category = bidirectional_iterator_tag;
            ...};
    ...};

template <typename T>
struct vector {
    struct iterator {
        using iterator_category = random_access_iterator_tag;
            ...};
    ...};
*/

template <typename BI>
void my_reverse_aux (BI b, BI e, bidirectional_iterator_tag) {
    while ((b != e) && (b != --e)) {
        swap(*b, *e);
        ++b;}}

template <typename RI>
void my_reverse_aux (RI b, RI e, random_access_iterator_tag) {
    while (b < --e) {
        swap(*b, *e);
        ++b;}}

/*
template <typename I>
struct my_iterator_traits {
    using iterator_category = typename I::iterator_category;};

template <typename T>
struct my_iterator_traits<T*> {
    using iterator_category = random_access_iterator_tag;};
*/

template <typename I>
void my_reverse (I b, I e) {
    my_reverse_aux(b, e, typename iterator_traits<I>::iterator_category());}

void test1 () {
    int a[] = {2, 3, 4};
    my_reverse(a, a);
    assert(equal(a, a + 3, begin({2, 3, 4})));}

void test2 () {
    int a[] = {2, 3, 4};
    my_reverse(a, a + 1);
    assert(equal(a, a + 3, begin({2, 3, 4})));}

void test3 () {
    vector<int>           x = {2, 3, 4};
    vector<int>::iterator e = begin(x);
    ++++e;
    my_reverse(begin(x), e);
    assert(equal(begin(x), end(x), begin({3, 2, 4})));}

void test4 () {
    vector<int> x = {2, 3, 4};
    my_reverse(begin(x), end(x));
    assert(equal(begin(x), end(x), begin({4, 3, 2})));}

void test5 () {
    list<int>           x = {2, 3, 4};
    list<int>::iterator e = begin(x);
    ++++e;
    my_reverse(begin(x), e);
    assert(equal(begin(x), end(x), begin({3, 2, 4})));}

void test6 () {
    list<int> x = {2, 3, 4};
    my_reverse(begin(x), end(x));
    assert(equal(begin(x), end(x), begin({4, 3, 2})));}

int main () {
    cout << "Reverse.cpp" << endl;
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    cout << "Done." << endl;
    return 0;}
