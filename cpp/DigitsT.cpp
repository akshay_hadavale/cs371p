// -----------
// DigitsT.cpp
// -----------

#include <utility> // !=

#include "gtest/gtest.h"

using namespace std;

using std::rel_ops::operator!=;

template <typename T>
class Digits {
    private:
        T _v;

    public:
        class iterator {
            private:
                T _v;

            public:
                iterator (const T& v) :
                        _v (v)
                    {}

                bool operator == (const iterator& rhs) const {
                    return (_v == rhs._v);}

                bool operator != (const iterator& rhs) const {
                    return !(*this == rhs);}

                const T operator * () const {
                    return _v % 10;}

                iterator& operator ++ () {
                    _v /= 10;
                    return *this;}

                const iterator operator ++ (int) {
                    iterator x = *this;
                    ++*this;
                    return x;}};

        Digits (const T& v) :
                _v (v)
            {}

        iterator begin () const {
            return iterator(_v);}

        iterator end () const {
            return iterator(0);}};

template <typename II1, typename II2>
bool my_equal (II1 b, II1 e, II2 c) {
    while (b != e) {
        if (*b != *c)
            return false;
        ++b;
        ++c;}
    return true;}

TEST(DigitsFixture, test1) {
    const Digits<int>           x = 0;
    const Digits<int>::iterator b = x.begin();
    const Digits<int>::iterator e = x.end();
    ASSERT_EQ(b, e);}

TEST(DigitsFixture, test2) {
    Digits<int>           x = 2;
    Digits<int>::iterator b = x.begin();
    Digits<int>::iterator e = x.end();
    ASSERT_NE(b, e);
    ASSERT_EQ(*b, 2);
    Digits<int>::iterator& p = ++b;
    ASSERT_EQ(&p, &b);
    ASSERT_EQ(b , e);}

TEST(DigitsFixture, test3) {
    Digits<int>           x = 23;
    Digits<int>::iterator b = x.begin();
    Digits<int>::iterator e = x.end();
    ASSERT_NE(b, e);
    ASSERT_EQ(*b, 3);
    ++b;
    ASSERT_NE(b, e);
    ASSERT_EQ(*b, 2);
    Digits<int>::iterator p = b++;
    ASSERT_NE(&p, &b);
    ASSERT_EQ(b , e);}

TEST(DigitsFixture, test4) {
    Digits<int>           x = 234;
    Digits<int>::iterator b = x.begin();
    Digits<int>::iterator e = x.end();
    ASSERT_TRUE(my_equal(b, e, begin({4, 3, 2})));}

TEST(DigitsFixture, test5) {
    Digits<int> x = 234;
    int s = 0;
    for (int v : x)
        s += v;
    ASSERT_EQ(s, 9);}
